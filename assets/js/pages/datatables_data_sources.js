/* ------------------------------------------------------------------------------
*
*  # Datatables data sources
*
*  Specific JS code additions for datatable_data_sources.html page
*
*  Version: 1.0
*  Latest update: Aug 1, 2015
*
* ---------------------------------------------------------------------------- */

$(function() {


    // Table setup
    // ------------------------------

    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        columnDefs: [{
            orderable: false,
            width: '100px',
            targets: [ 5 ]
        }],
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        },

    });

    // AJAX sourced data
    var table;
    table = $('.datatable-ajax').dataTable({
        // ajax: 'assets/demo_data/tables/datatable_ajax.json'
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "ajax": {
          "url" : 'http://localhost/himatifta/c_json/data_pengurus',
          "type": "POST"
        },
        "columns": [
            {"data": "NAMA_PENGURUS"},
            {"data": "NBI_PENGURUS"},
            {"data": "ALAMAT1_PENGURUS"},
            {"data": "TELP1_PENGURUS"},
            {"data": "NAMA_JABATAN"},
            {"data": "PERIODE_PENGURUS"},
            {"data": "TELP1_PENGURUS"},
            {"data": "NAMA_JABATAN"},
            {"data": "PERIODE_PENGURUS"},
            {"data": "TELP1_PENGURUS"},
            {"data": "NAMA_JABATAN"},
            {"data": "PERIODE_PENGURUS"}
        ],
        "scrollX" : true
    });

});
