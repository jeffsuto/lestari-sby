var base_url = 'http://localhost/lestary-sby/';

$('#print_laporanpengiriman').click(function(){
  var year = $('.year').val();
  var month = $('.month').val();
  var day = $('.day').val();
  var url = base_url + 'admin/laporan/pengiriman/print?year='+year+'&month='+month+'&day='+day;
  window.open( url, '_blank');
});

$('#print_laporanpembelian').click(function(){
  var year = $('.year').val();
  var month = $('.month').val();
  var day = $('.day').val();
  var url = base_url + 'admin/laporan/pembelian/print?year='+year+'&month='+month+'&day='+day;
  window.open( url, '_blank');
});

$('#print_laporanpenjualan').click(function(){
  var year = $('.year').val();
  var month = $('.month').val();
  var day = $('.day').val();
  var url = base_url + 'admin/laporan/penjualan/print?year='+year+'&month='+month+'&day='+day;
  window.open( url, '_blank');
});
